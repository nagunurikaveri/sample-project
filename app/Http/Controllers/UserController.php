<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Validator;

class UserController extends Controller
{
    public function registration(Request $req){
        $validation = Validator::make($req->all(),[
            'name'=>'required',
            'email'=>'required|email|unique:users',
            'password'=>'required',
            'c_password'=>'required|same:password',


        ]);
        if($validation->fails()){
            return response()->json($validation->errors(),202);
        }
        $allData = $req->all();
        $allData['password'] = bcrypt($allData['password']);
        $user = User::create($allData);
        $resArr = [];
        $resArr['token']=$user->createToken('api-application')->accessToken;
        $resArr['name'] = $user->name;
        return response()->json($resArr,200);

    }
    public function login(Request $req){
        if(Auth::attempt([
            'email'=>$req->email,
            'password'=>$req->password
        ])){
            $user = Auth::user();
            $resArr = [];
            $resArr['token']=$user->createToken('api-application')->accessToken;
            $resArr['name'] = $user->name;
            return response()->json($resArr,200);
        }else {
            return response()->json(['error'=>'Unauthorized user'],203);

        }

    }
    public function info(){
        $data = User::all();
        $resArr = [
            "status"=>'ok',
            "data"=>$data
        ];
        return response()->json($resArr,200);
    }
    public function logout(Request $req){
        $req->user()->token()->revoke();
        return response()->json(["message"=>"Successfully log out"]);
    }
}
