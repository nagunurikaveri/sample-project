<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PostController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/register',[UserController::class,'registration']);
Route::post('/login',[UserController::class,'login']);
Route::get('/login',[UserController::class,'login'])->name('login');

Route::middleware('auth:api')->get('/details',[UserController::class,'info']);
Route::middleware('auth:api')->get('/logout',[UserController::class,'logout']);

Route::middleware('auth:api')->group(function () {
    Route::post('/posts',[PostController::class,'create']);
    Route::get('/posts/{id}',[PostController::class,'get']);
    Route::put('/posts/{id}',[PostController::class,'update']);
    Route::delete('/posts/{id}',[PostController::class,'delete']);
    Route::get('/posts',[PostController::class,'index']);
});
